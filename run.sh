#!/bin/bash

HOME_PATH="$(dirname "$(realpath "$0")")"

# create xauth for docker
XSOCK=/tmp/.X11-unix
XAUTH=/tmp/.docker.xauth
if [ ! -f $XAUTH ]; then
	touch $XAUTH
	xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -
fi

DOCKER_ARGS=(-v "${XSOCK}:${XSOCK}:rw" -v "${XAUTH}:${XAUTH}:rw" -e "XAUTHORITY=${XAUTH}" -e "DISPLAY=${DISPLAY}")
SRC_VOLUME="${HOME_PATH}/images:/home/app/images"

docker run -it --rm --name itoai-assignment2 ${DOCKER_ARGS[@]} -v "$SRC_VOLUME" itoai-assignment2 ./AI_HW2 ${@:1}