#pragma once

/*
Bot module.

This module is responsible for working with Bots: creation, crossovering, compiling.
Compilation of bots - the process of generation user-friendly data from DNA of bots.
*/

#include <vector>
#include <ostream>

#include <opencv2/opencv.hpp>


/*
	Schema of bots.

	Contains number of static and dynamic points and aspect ratio.
*/
class BotSchema
{
private:
	int dynamic_npoints_;
	int static_points_;
	double aspect_ratio_;  // w/h
	std::vector<double> get_static_points_;

public:
	BotSchema() {};
	BotSchema(int npoints, double aspect_ratio);

	int dynamic_npoints() const { return dynamic_npoints_; }
	int static_points() const { return static_points_; }
	double aspect_ratio() const { return aspect_ratio_; }

	const std::vector<double>& get_static_points() { return get_static_points_; }
};


class Bot
{
public:
	constexpr static double MUTATION_RATE = 0.001;
	constexpr static double MUTATION_SOFT_K = 0.03;

private:
	BotSchema* schema;
	std::vector<double> points;

	Bot(BotSchema* schema, const std::vector<double>& points) : schema(schema), points(points) {};

public:
	static Bot random(BotSchema* schema);

	// cross two bots to get a chindren
	Bot operator +(const Bot& bot) const;

	// extract points from bot
	std::vector<double> compile() const;
};
