/*
	Main module.

	This module is responsible for parsing command line parameters, starting and managing the evolution process
*/


#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <iostream>

#include <boost/program_options.hpp>
#include <boost/log/trivial.hpp>
#include <opencv2/opencv.hpp>

#include "loss_estimator.hpp"
#include "painter.hpp"
#include "bot.hpp"
#include "generation.hpp"


namespace po = boost::program_options;


uint64_t getCTimeNanosecond()
{
  timespec time;
  clock_gettime(CLOCK_REALTIME, &time);
  return (uint64_t)(time.tv_sec) * 1000000000 + time.tv_nsec;
}

struct GlobalConfig
{
	std::string input_file, output_file, mask_file;
	bool has_mask = false;
	bool quiet_mode = false;
	double input_scale = 1, output_scale = 1;

	int time_limit = -1;
	int iterations = 1000, points = 1500, pop_size = 160;
	int seed;

	int number_of_workers = 1;
	int show_each = 0;

	std::string save_bests_to;
	int save_bests_each = 0;
};

// Run program
void process(const GlobalConfig& cfg)
{
	// Set seed
	srand(cfg.seed);
	printf("[INFO] Seed = %d\n", cfg.seed);

	printf("[DEBUG] Reading input image '%s'\n", cfg.input_file.c_str());
	cv::Mat target = cv::imread(cfg.input_file);
	cv::resize(target, target, cv::Size(), cfg.input_scale, cfg.input_scale, cv::INTER_AREA);
	if (target.empty())
		throw std::runtime_error("Failed to load input image");
	
	BotSchema schema(cfg.points, target.cols / (double)target.rows);
	Painter painter(target);
	LossEstimator loss_estimator;

	if (cfg.has_mask)
	{
		printf("[DEBUG] Reading mask image '%s'\n", cfg.mask_file.c_str());
		cv::Mat mask = cv::imread(cfg.mask_file);
		if (mask.empty())
			throw std::runtime_error("Failed to load mask image");
		if (mask.channels() == 3)
			cv::cvtColor(mask, mask, cv::COLOR_BGR2GRAY);
		cv::resize(mask, mask, cv::Size(), cfg.input_scale, cfg.input_scale, cv::INTER_AREA);
		if (target.rows != mask.rows || target.cols != mask.cols)
			throw std::runtime_error("Size of mask is not equal to size of input image");

		loss_estimator = LossEstimator(target, mask, &painter);
	}
	else
	{
		printf("[DEBUG] No mask image will be used\n");
		loss_estimator = LossEstimator(target, &painter);
	}

	if (cfg.show_each > 0)
	{
		printf("[DEBUG] Opening window\n");
		cv::namedWindow("image");
		cv::waitKey(33);
	}

	printf("[DEBUG] Starting %d workers\n", cfg.number_of_workers);
	Generation::start_workers(cfg.number_of_workers);

	uint64_t start_time = getCTimeNanosecond();
	Generation gen;

	int iter = 0;
	while (true)
	{
		// Iteration limit
		if (cfg.iterations >= 0 && iter >= cfg.iterations) break;
		// Time limit
		if (cfg.time_limit >= 0 && getCTimeNanosecond() - start_time > cfg.time_limit * (uint64_t)1e9) break;

		// Generate next population/create new one and estimate all bots using loss_estimator
		if (iter == 0)
			gen = Generation::random_generation(cfg.pop_size, &schema, &loss_estimator);
		else
			gen = gen.net_gen(cfg.pop_size, &loss_estimator);

		if (!cfg.quiet_mode)
		{
			// Print logs
			if (cfg.time_limit < 0 && cfg.iterations < 0)  // no limits
			{
				printf("[INFO] Iter: %d,  Score: %.6f,  ETA: unlimit\n", iter+1, gen.get_best_score());	
			}
			else
			{
				double dt = (getCTimeNanosecond() - start_time) / 1e9;
				int total_iters = cfg.iterations;
				double total = dt * cfg.iterations / (iter+1);
				if (cfg.iterations < 0 || (cfg.time_limit >= 0 && cfg.time_limit < total))  // time limit
				{
					total = cfg.time_limit;
					total_iters = round((iter+1) / dt * total);
				}
				double eta = std::max(0.0, total - dt);
				total_iters = std::max(total_iters, iter+1);
				printf("[INFO] Iter: %d/%d,  Score: %.6f,  ETA: %d min %d sec\n", iter+1, total_iters, gen.get_best_score(), (int)eta / 60, ((int)eta) % 60);
			}
		}

		if (cfg.show_each > 0 && iter % cfg.show_each == 0)
		{
			// Show image
			cv::imshow("image", painter.paint(gen.get_best_bot().compile(), target.rows, target.cols));
			cv::waitKey(33);
		}

		if (cfg.save_bests_each > 0 && iter % cfg.save_bests_each == 0)
		{
			// Save image sequence
			char buffer[cfg.save_bests_to.length() + 16];
			sprintf(buffer, "%s/%08d.png", cfg.save_bests_to.c_str(), iter);
			cv::imwrite(buffer, painter.paint(gen.get_best_bot().compile(), target.rows * cfg.output_scale, target.cols * cfg.output_scale));
		}

		iter++;
	}

	printf("[INFO] Complete in %lu sec\n", (getCTimeNanosecond() - start_time) / ((int)1e9));
	printf("[INFO] Save result to file %s\n", cfg.output_file.c_str());
	cv::imwrite(cfg.output_file, painter.paint(gen.get_best_bot().compile(), target.rows * cfg.output_scale, target.cols * cfg.output_scale));

	printf("[DEBUG] Stoping workers\n");
	Generation::stop_workers();
}

void assert_cfg(const GlobalConfig& cfg)
{
	assert(cfg.iterations != 0 && cfg.points > 0 && cfg.pop_size > 0);
	assert(cfg.input_scale > 0 && cfg.output_scale > 0);
	assert(cfg.number_of_workers > 0);
	assert(cfg.show_each >= 0 && cfg.save_bests_each >= 0);
}

int main(int argc, char** argv)
{
	GlobalConfig cfg;

	// Parsing commandline agruments
	po::options_description desc_general("General options");
	desc_general.add_options()
		("help,h", "show help and exit")
		("input,i", po::value<std::string>(&cfg.input_file)->value_name("FILE_NAME"), "set input file")
		("output,o", po::value<std::string>(&cfg.output_file)->value_name("FILE_NAME"), "set output file")
		("mask,m", po::value<std::string>(&cfg.mask_file)->value_name("FILE_NAME"), "set loss mask")
		("quiet,q", "quiet mode")
		("input-scale", po::value<double>(&cfg.input_scale)->value_name("SCALE")->default_value(cfg.input_scale), "scale input image and mask before start")
		("output-scale", po::value<double>(&cfg.output_scale)->value_name("SCALE")->default_value(cfg.output_scale), "scale output images");

	po::options_description desc_algorithm("Algorithm options");
	desc_algorithm.add_options()
		("seed", po::value<int>(&cfg.seed)->value_name("SEED"), "set seed")
		("iter", po::value<int>(&cfg.iterations)->value_name("ITER")->default_value(cfg.iterations), "set iterations")
		("time-limit", po::value<int>(&cfg.time_limit)->value_name("SEC")->default_value(cfg.time_limit), "set maximum time program can execute (in seconds)")
		("pop-size", po::value<int>(&cfg.pop_size)->value_name("POP_SIZE")->default_value(cfg.pop_size), "set the number of bots in one generation")
		("points", po::value<int>(&cfg.points)->value_name("POINTS")->default_value(cfg.points), "set numper of points on image");

	po::options_description desc_other("Other options");
	desc_other.add_options()
		("jobs,j", po::value<int>(&cfg.number_of_workers)->value_name("JOBS")->default_value(cfg.number_of_workers), "set the number of workers (threads)")
		("show-each", po::value<int>(&cfg.show_each)->value_name("ITER")->default_value(cfg.show_each), "set period of showing images in GUI (0 - do not show)")
		("save-bests-each", po::value<int>(&cfg.save_bests_each)->value_name("ITER")->default_value(cfg.save_bests_each), "save best images each ITER iterations (0 - do not save)")
		("save-bests-to", po::value<std::string>(&cfg.save_bests_to)->value_name("DIR_NAME"), "save best images to folder");

	po::options_description desc;
	desc.add(desc_general).add(desc_algorithm).add(desc_other);


	auto parser = po::command_line_parser(argc, argv).options(desc);
	po::variables_map vm;
	po::store(parser.run(), vm);
	po::notify(vm);

	if (vm.count("help"))
	{
		std::cout << "Usage: " << argv[0] << " [options...]" << std::endl;
		std::cout << "Genetic algorithm for generating images (by Markovvn1)" << std::endl;
		std::cout << desc;
		return 0;
	}

	if (!vm.count("input"))
	{
		printf("Error: input file was not specified (see --help)\n");
		return 1;
	}

	if (!vm.count("output"))
	{
		printf("Error: output file was not specified (see --help)\n");
		return 1;
	}

	cfg.has_mask = vm.count("mask") > 0;
	cfg.quiet_mode = vm.count("quiet") > 0;

	if (!vm.count("seed"))
	{
		srand(getCTimeNanosecond());
		cfg.seed = rand();
	}

	assert_cfg(cfg);
	process(cfg);  // run program

	return 0;
}
