#pragma once

#include "painter.hpp"
#include "bot.hpp"


class LossEstimator
{
private:
    cv::Mat target, mask;
    Painter* painter;
    bool has_mask;

public:
    LossEstimator() : has_mask(false) {}
    LossEstimator(cv::Mat target, Painter* painter) : target(target), painter(painter), has_mask(false) {}
    LossEstimator(cv::Mat target, cv::Mat mask, Painter* painter) : target(target), mask(mask), painter(painter), has_mask(true) {}

    double estimate(const Bot& bot) const;
};