#pragma once

/*
    Generation module.

    This module is responsible for managing the population of bots.
    This module deside which bots take to next generation, which bots to breed with which bots
*/

#include "bot.hpp"
#include "loss_estimator.hpp"

#include <thread>
#include <vector>


class Generation
{
private:
    LossEstimator* loss_estimator;
    std::vector<std::pair<double, Bot> > bots; // sorted. bots[0] - the best

    void send_job();
    void sort_bots();

    Generation(LossEstimator* loss_estimator) : loss_estimator(loss_estimator) {}

public:
    Generation() {}

    static void start_workers(int num_workers);
    static void stop_workers();

    static Generation random_generation(int population_size, BotSchema* schema, LossEstimator* loss_estimator);

    double get_best_score() { return bots[0].first; };
    Bot get_best_bot() { return bots[0].second; };

    Generation net_gen(int population_size, LossEstimator* loss_estimator);
};