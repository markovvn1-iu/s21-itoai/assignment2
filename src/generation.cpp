#include "generation.hpp"

#include <algorithm>
#include <mutex>
#include <condition_variable>


struct ThreadStruct
{
    int thread_num;
    LossEstimator* estimator;

    std::vector<std::pair<double, Bot> >* jobs;
    std::mutex* new_job;
    std::mutex unlocker;

    std::mutex data_lock;
    int notdone_count;

    bool working = false;
    std::vector<std::thread> workers;
};

ThreadStruct thr;

void worker(int thr_idx)
{
    thr.new_job[thr_idx].lock();

    while (thr.working)
    {
        // notify main process that job is done
        thr.data_lock.lock();
        thr.notdone_count--;
        if (thr.notdone_count == 0) thr.unlocker.unlock();
        thr.data_lock.unlock();

        // wait for new job
        thr.new_job[thr_idx].lock();
        if (!thr.working) break;

        // get new job
        std::vector<std::pair<double, Bot> >& jobs = *thr.jobs;

        // execute job
        for (int i = thr_idx; i < jobs.size(); i += thr.thread_num)
            jobs[i].first = thr.estimator->estimate(jobs[i].second);
    }

    thr.new_job[thr_idx].unlock();
}

void send_job(std::vector<std::pair<double, Bot> >& jobs, LossEstimator* estimator)
{
    if (!thr.working) throw std::runtime_error("No workers running");

    thr.jobs = &jobs;
    thr.estimator = estimator;
    thr.notdone_count = thr.thread_num;

    // wait untill all threads will finish
    for (int i = 0; i < thr.thread_num; i++)
        thr.new_job[i].unlock();
    thr.unlocker.lock();
}




void Generation::send_job()
{
    ::send_job(bots, loss_estimator);
}

bool compare_first(const std::pair<double, Bot>& a, const std::pair<double, Bot>& b)
{
    return a.first < b.first;
}

void Generation::sort_bots()
{
    sort(bots.begin(), bots.end(), compare_first);
}

void Generation::start_workers(int num_workers)
{
    if (thr.working) return;

    thr.thread_num = num_workers;

    thr.working = true;
    thr.notdone_count = thr.thread_num;
    thr.unlocker.lock();

    thr.new_job = new std::mutex[thr.thread_num];

    // start threads
    for (int i = 0; i < thr.thread_num; i++)
        thr.workers.push_back(std::thread(worker, i));

    // wait until all threads ready to take jobs
    thr.unlocker.lock();
}

void Generation::stop_workers()
{
    if (!thr.working) return;

    thr.working = false;
    thr.notdone_count = thr.thread_num;
    for (int i = 0; i < thr.thread_num; i++)
        thr.new_job[i].unlock();

    for (int i = 0; i < thr.thread_num; i++)
        thr.workers[i].join();

    thr.workers.clear();
    delete [] thr.new_job;
}

Generation Generation::random_generation(int population_size, BotSchema* schema, LossEstimator* loss_estimator)
{
    Generation res(loss_estimator);

    res.bots.reserve(population_size);
    for (int i = 0; i < population_size; i++)
        res.bots.push_back({0, Bot::random(schema)});

    res.send_job();
    res.sort_bots();

    return res;
}

Generation Generation::net_gen(int population_size, LossEstimator* loss_estimator)
{
    Generation res(loss_estimator);

    for (int i = 0; i < 1; i++)
        res.bots.push_back({0, bots[i].second});

    for (int i = res.bots.size(); i < population_size; i++)
        res.bots.push_back({0, bots[rand() % (bots.size() / 10)].second + bots[rand() % (bots.size() / 2)].second});

    res.send_job();
    res.sort_bots();
    
    return res;
}