#include "painter.hpp"

#include <delaunator-cpp/delaunator.hpp>


Painter::TriangleMesh Painter::triangulate(const PointCloud& points)
{
	// use third-party library (as it is not a part of assignment)
	delaunator::Delaunator d(points);
    return d.triangles;
}

// Old version of colorize function (slow)
/*
TriangleColor Painter::colorize(const cv::Mat& target, const PointCloud& points, const TriangleMesh& mesh)
{
	std::vector<cv::Point> cv_points;
	cv_points.reserve(points.size());

	double s = target.cols * 64; // shift factor
	for (int i = 0; i < points.size(); i += 2)
		cv_points.push_back({int(points[i] * s), int(points[i+1] * s)});

	TriangleColor res;

    cv::Point pts[3];
    const cv::Point* ppt1[1] = { pts };
    int npt = 3;

	cv::Mat1b mask(target.rows, target.cols);
    for (int i = 0; i < mesh.size(); i += 3)
    {
		pts[0] = cv_points[mesh[i+0]];
		pts[1] = cv_points[mesh[i+1]];
		pts[2] = cv_points[mesh[i+2]];

        mask = 0;
        cv::fillPoly(mask, ppt1, &npt, 1, cv::Scalar(255), cv::LINE_AA, 6);
        res.push_back(cv::mean(target, mask));
    }

	return res;
}
*/

// Get color of single triangle
cv::Scalar getColor(const cv::Mat& target, const cv::Point2d& p1, const cv::Point2d& p2, const cv::Point2d& p3)
{
	int min_x = std::min(std::min(p1.x, p2.x), p3.x);
	int max_x = std::max(std::max(p1.x, p2.x), p3.x);
	int min_y = std::min(std::min(p1.y, p2.y), p3.y);
	int max_y = std::max(std::max(p1.y, p2.y), p3.y);

	min_x = std::max(min_x - 2, 0);
	min_y = std::max(min_y - 2, 0);
	max_x = std::min(max_x + 2, target.cols - 1);
	max_y = std::min(max_y + 2, target.rows - 1);

	cv::Point pts[3] = {
		{(int)((p1.x - min_x) * 64), (int)((p1.y - min_y) * 64)},
		{(int)((p2.x - min_x) * 64), (int)((p2.y - min_y) * 64)},
		{(int)((p3.x - min_x) * 64), (int)((p3.y - min_y) * 64)}};
    const cv::Point* ppt1[1] = { pts };
    int npt = 3;

	cv::Mat1b mask = cv::Mat1b::zeros(max_y - min_y + 1, max_x - min_x + 1);
	cv::fillPoly(mask, ppt1, &npt, 1, cv::Scalar(255), cv::LINE_AA, 6);

	/*
	// Looks like it is slower

	uint64_t sum_r = 0, sum_g = 0, sum_b = 0;
    uint64_t sum_m = 0;

	int start_offset = (min_y * target.cols + min_x) * target.channels();
    for (int y = 0; y < mask.rows; y++)
        for (int x = 0; x < mask.cols; x++)
        {
            int mask_pix = mask.data[y * mask.cols + x];
            uint8_t* img_pix = target.data + start_offset + (y * target.cols + x) * 3;
            sum_m += mask_pix;
            sum_b += ((int)img_pix[0]) * mask_pix;
            sum_g += ((int)img_pix[1]) * mask_pix;
            sum_r += ((int)img_pix[2]) * mask_pix;
        }

	return cv::Scalar(sum_b / (double)sum_m, sum_g / (double)sum_m, sum_r / (double)sum_m);
	*/
	return cv::mean(target(cv::Rect(min_x, min_y, mask.cols, mask.rows)), mask);
}

Painter::TriangleColor Painter::colorize(const PointCloud& points, const TriangleMesh& mesh)
{
	TriangleColor res;

    for (int i = 0; i < mesh.size(); i += 3)
    {
		cv::Point2d p1(points[2 * mesh[i+0] + 0] * target.cols, points[2 * mesh[i+0] + 1] * target.rows);
		cv::Point2d p2(points[2 * mesh[i+1] + 0] * target.cols, points[2 * mesh[i+1] + 1] * target.rows);
		cv::Point2d p3(points[2 * mesh[i+2] + 0] * target.cols, points[2 * mesh[i+2] + 1] * target.rows);

        res.push_back(getColor(target, p1, p2, p3));
    }

	return res;
}

cv::Mat Painter::draw(int height, int width, const PointCloud& points, const TriangleMesh& mesh, const TriangleColor& color)
{
	std::vector<cv::Point> cv_points;
	cv_points.reserve(points.size());

	double s = width * 64; // shift factor
	for (int i = 0; i < points.size(); i += 2)
		cv_points.push_back({int(points[i] * s), int(points[i+1] * s)});


    cv::Point pts[3];
    const cv::Point* ppt1[1] = { pts };
    int npt = 3;

	cv::Mat res(height, width, CV_8UC3);
    for (int i = 0; i < mesh.size() / 3; i++)
    {
        pts[0] = cv_points[mesh[3*i+0]];
		pts[1] = cv_points[mesh[3*i+1]];
		pts[2] = cv_points[mesh[3*i+2]];

        cv::fillPoly(res, ppt1, &npt, 1, color[i], cv::LINE_AA, 6);
    }

	return res;
}




cv::Mat Painter::paint(const std::vector<double>& points, int height, int width)
{
    TriangleMesh mesh = triangulate(points);
	TriangleColor color = colorize(points, mesh);
	return draw(height, width, points, mesh, color);
}

