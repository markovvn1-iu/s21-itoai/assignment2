#include "bot.hpp"

#include <cmath>


BotSchema::BotSchema(int npoints, double aspect_ratio)
{
    // calculate number of static and dynamic points
    static_points_ = int(sqrt(npoints) / 2) * 2;
    if (static_points_ < 4) static_points_ = 4;
    if (static_points_ >= npoints) throw std::runtime_error("static_points_ >= npoints");
    dynamic_npoints_ = npoints - static_points_;
    aspect_ratio_ = aspect_ratio;

    get_static_points_.reserve(static_points_ * 2);

    // calculate number of static points in height and in width
    int free_points = (static_points_ - 4) / 2;
    int h_points = 1 + round(free_points / (aspect_ratio + 1));
    int w_points = 2 + free_points - h_points;

    // generate coordinates of static points
    for (int i = 0; i < w_points; i++)
	{
        double t = i / (double)w_points;
        get_static_points_.push_back(t);  // x
        get_static_points_.push_back(0);  // y
        get_static_points_.push_back(1 - t);  // x
        get_static_points_.push_back(1);  // y
    }

    for (int i = 0; i < h_points; i++)
	{
        double t = i / (double)h_points;
        get_static_points_.push_back(0);  // x
        get_static_points_.push_back(1 - t);  // y
        get_static_points_.push_back(1);  // x
        get_static_points_.push_back(t);  // y
    }
}



Bot Bot::random(BotSchema* schema)
{
    int npoints = schema->dynamic_npoints();

    std::vector<double> points;
    points.reserve(npoints * 2);
	for (int i = 0; i < npoints * 2; i++)
		points.push_back(rand() / (double)RAND_MAX);

    return Bot(schema, points);
}

Bot Bot::operator +(const Bot& bot) const
{
    if (schema != bot.schema) throw std::runtime_error("schema != bot.schema");
    if (points.size() != bot.points.size()) throw std::runtime_error("points.size() != bot.points.size()");

    std::vector<double> new_points = points;
	
    // crossover
    for (int i = 0; i < points.size(); i += 2)
        if (rand() < RAND_MAX / 2)
        {
            new_points[i] = bot.points[i];
            new_points[i+1] = bot.points[i+1];
        }

    // mutation
    for (int i = 0; i < points.size(); i++)
    {
        if (rand() > RAND_MAX * MUTATION_RATE) continue;

        if (rand() < RAND_MAX / 8)
        {
            // normal mutation
            new_points[i] = rand() / (double)RAND_MAX;
        }
        else
        {
            // soft mutation
            double new_v = rand() / (double)RAND_MAX;
            if (i % 2 == 1) new_v *= schema->aspect_ratio();
            new_points[i] += new_v * MUTATION_SOFT_K;
            if (new_points[i] < 0) new_points[i] = 0;
            if (new_points[i] > 1) new_points[i] = 1;
        }
    }
    return Bot(schema, new_points);
}

std::vector<double> Bot::compile() const
{
    std::vector<double> res = schema->get_static_points();
    res.reserve(res.size() + points.size());
    res.insert(res.end(), points.begin(), points.end());
    return res;
}
