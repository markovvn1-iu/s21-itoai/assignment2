#pragma once

/*
    Painter module.

    This module is responsible for rendering bots.
*/


#include <opencv2/opencv.hpp>


class Painter
{
typedef std::vector<double> PointCloud;
typedef std::vector<std::size_t> TriangleMesh;
typedef std::vector<cv::Scalar> TriangleColor;

private:
	cv::Mat target;

	// Use delaunay triangulation to triangulate given points
	TriangleMesh triangulate(const PointCloud& points);

	// Determine color of each triangle
	TriangleColor colorize(const PointCloud& points, const TriangleMesh& mesh);

	// Draw given triangles with given colors
	cv::Mat draw(int height, int width, const PointCloud& points, const TriangleMesh& mesh, const TriangleColor& color);

public:
	Painter() {}
	Painter(cv::Mat target) : target(target) {}

	// Draw image of size (height, width) from given points
	cv::Mat paint(const std::vector<double>& points, int height, int width);
};