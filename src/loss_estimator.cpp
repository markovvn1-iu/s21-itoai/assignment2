#include "loss_estimator.hpp"


double LossEstimator::estimate(const Bot& bot) const
{
    cv::Mat res = painter->paint(bot.compile(), target.rows, target.cols);
    uint64_t r = 0, rm = 0;

    for (int y = 0; y < target.rows; y++)
        for (int x = 0; x < target.cols; x++)
        {
            uchar* a = res.data + (y * target.cols + x) * 3;
            uchar* b = target.data + (y * target.cols + x) * 3;
            uint32_t m = 255;
            if (has_mask)
            {
                m = mask.data[y * target.cols + x];
                m = m * m;
            }
            rm += m * 3;
            r += abs(a[0] - b[0]) * m;
            r += abs(a[1] - b[1]) * m;
            r += abs(a[2] - b[2]) * m;
        }

    return r / (double)rm;
    // return cv::norm(res, target, cv::NORM_L2, mask);
}