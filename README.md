# Assignment 2

Genetic algorithm for generating images. See [report.pdf](report.pdf) for details of implementation.

## Run in docker

First, build the image

```bash
./build.sh
```

Then put your picture to `images` folder (this folder will be mounted to docker). After this you can run program. See help:

```
./run.sh --help
```

Noice: you can pass argument `--show-each 10` to show image in GUI each 10 iterations. See `--help` for more details

## Run locally (and develop)

You need to install opencv >=4.5, libboost-program-options-dev. You can use VSCode for development.

## Examples

![](.gitlab/01-02.png)
```bash
./run.sh -i images/input/01.png -o images/output/01.png --seed 1485246670 --points 1500 --iter 15000 -j4

./run.sh -i images/input/02.png -o images/output/02.png --seed 1418538997 --points 1500 --iter 15000 -j4
```

![](.gitlab/03-04.png)
```bash
./run.sh -i images/input/03.png -m images/input/03_m.png -o images/output/03.png --seed 1400581748 --points 1500 --iter 15000 -j4

./run.sh -i images/input/04.png -o images/output/04.png --seed 1664090156 --points 1500 --iter 15000 -j4
```

![](.gitlab/06-07.png)
```bash
./run.sh -i images/input/06.png -o images/output/06.png --seed 1381952969 --points 1500 --iter 15000 -j4

./run.sh -i images/input/07.png -m images/input/07_m.png -o images/output/07.png --seed 1220418050 --points 1500 --iter 15000 -j4
```

![](.gitlab/09-10.png)
```bash
./run.sh -i images/input/09.png -m images/input/09_m.png -o images/output/09.png --seed 80462562 --points 1500 --iter 15000 -j4

./run.sh -i images/input/10.png -m images/input/10_m.png -o images/output/10.png --seed 3081186 --points 1500 --iter 15000 -j4
```

![](.gitlab/11-12.png)
```bash
./run.sh -i images/input/11.png -m images/input/11_m2.png -o images/output/11.png --seed 1532884916 --points 1500 --iter 15000 -j4

./run.sh -i images/input/12.png -m images/input/12_m.png -o images/output/12.png --seed 1754678990 --points 1500 --iter 15000 -j4
```