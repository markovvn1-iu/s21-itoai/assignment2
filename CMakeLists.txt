cmake_minimum_required(VERSION 3.0.1)

project(AI_HW2)

if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()

SET(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS_RELEASE "-O3")

set(Boost_USE_STATIC_LIBS ON)

find_package( OpenCV COMPONENTS core highgui imgcodecs imgproc REQUIRED )
find_package( Threads )
find_package( Boost COMPONENTS program_options REQUIRED )

include_directories(third_party src ${CMAKE_SOURCE_DIR} ${OpenCV_INCLUDE_DIRS} ${Boost_INCLUDE_DIR})

file(GLOB_RECURSE SRC src/*.cpp)


add_executable(AI_HW2 ${SRC})
target_link_libraries(AI_HW2 ${OpenCV_LIBS} Threads::Threads ${Boost_LIBRARIES})
