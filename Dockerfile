FROM opencvcourses/opencv-docker:4.5.1

# Install building requirements
RUN set -ex \
	&& apt-get -qq update \
	&& apt-get -qq install libcanberra-gtk-module libboost-program-options-dev \
	&& apt-get -qq clean \
	&& rm -rf /var/lib/apt/lists/*


ARG USER_UID=1000
ARG USER_GID=1000

# Create non-root user
RUN set -ex \
	&& groupadd -g ${USER_GID} app \
	&& useradd -m -d /home/app -u ${USER_UID} -s /bin/bash -g app -G sudo app


# Copy sources
COPY src /root/ws/src
COPY third_party /root/ws/third_party
COPY CMakeLists.txt /root/ws


# Build project
RUN set -ex \
	&& cd /root/ws \
	&& mkdir build && cd build \
	&& cmake .. && make -j8 \
	&& mv ./AI_HW2 /home/app/ \
	&& chown app:app /home/app/AI_HW2


# Create entrypoint (catch Ctrl+C)
RUN echo '#!/bin/bash\n$@ &\nPID=`jobs -p`\n\ntrap "kill -SIGQUIT $PID" INT\nwait' > /entrypoint.sh \
	&& chmod +x /entrypoint.sh


USER app
WORKDIR /home/app
ENTRYPOINT ["/entrypoint.sh"]