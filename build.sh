#!/bin/bash

realpath() {  # for MAC OS
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}


cd "$(dirname "$(realpath "$0")")"


if [[ "$OSTYPE" == "darwin"* ]]; then
	USER_UID=$(stat -f '%u' src)
	USER_GID=$(stat -f '%g' src)
else
	USER_UID=$(stat -c '%u' src)
	USER_GID=$(stat -c '%g' src)
fi

if [ $USER_UID -eq 0 ] || [ $USER_GID -eq 0 ]; then
    echo "You are trying to create non-root user with UID=$USER_UID and GID=$USER_GID. You probably doing something wrong"
    exit 1
fi

docker build --build-arg USER_UID=$USER_UID --build-arg USER_GID=$USER_GID -t itoai-assignment2 .